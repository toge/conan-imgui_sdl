#include "SDL.h"
#undef main

#include "imgui.h"
#include "imgui_sdl.h"

int main() {
    ImGui::CreateContext();
    ImGuiSDL::Deinitialize();
}
