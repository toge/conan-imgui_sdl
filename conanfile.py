from conans import ConanFile, CMake
import shutil

class ImguiSdlConan(ConanFile):
    name = "imgui_sdl"
    version = "0.0-20200131"
    license = "MIT"
    author = "toge.mail@gmail.com"
    url = "https://bitbucket.org/toge/conan-imgui_sdl/"
    homepage = "https://github.com/Tyyppi77/imgui_sdl/"
    description = "ImGuiSDL: SDL2 based renderer for Dear ImGui "
    topics = ("SDL", "dear imgui", "GUI")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": False}
    generators = "cmake"
    exports = "CMakeLists.txt"
    requires = ("imgui/[>= 1.74]", "sdl2/[>= 2.0.7]@bincrafters/stable")

    def source(self):
        self.run("git clone https://github.com/Tyyppi77/imgui_sdl/")
        self.run("cd imgui_sdl && git checkout 0812f5ed05c0eb801fdb8ae71eb0cc75c95a8cff")
        shutil.copyfile("CMakeLists.txt", "imgui_sdl/CMakeLists.txt")

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="imgui_sdl")
        cmake.build()

    def package(self):
        self.copy("*.h", dst="include", src="imgui_sdl")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["imgui_sdl"]

